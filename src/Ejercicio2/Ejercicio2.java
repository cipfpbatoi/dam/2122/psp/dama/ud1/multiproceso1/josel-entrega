package Ejercicio2;

import java.io.*;
import java.util.*;

public class Ejercicio2 {
    public static void main(String[] args) throws IOException {
        //en este bloque se le asigna a una string la ruta en la que esta el jar, y conviertes ese string en una array list
        String comando = "java -jar out/artifacts/Ejercicio1_jar/Ejercicio1.jar";
        List<String> lista = new ArrayList<>(Arrays.asList(comando.split(" ")));
        //se crea el proceso y el fichero randoms.txt
        ProcessBuilder probui = new ProcessBuilder(lista);
        File file = new File("randoms.txt");
        //BufferedWriter bfw = new BufferedWriter(new FileWriter(file));

        //en este try lo usaremos para inicializar el proceso y se indica que se va a escribir en el fichero anterior
        try (BufferedWriter bfw = new BufferedWriter(new FileWriter(file))){
            //se inicia el proceso y se inicializa que se pueda escribir en el fichero
            Process process = probui.start();
            OutputStream outstr = process.getOutputStream();
            OutputStreamWriter outstrwri = new OutputStreamWriter(outstr);
            BufferedWriter bufwri = new BufferedWriter(outstrwri);

            //se crean dos escaneres para poder obtener las palabras que se pasen
            Scanner scanner =  new Scanner(process.getInputStream());
            Scanner scanner1 =  new Scanner(System.in);
            String linea = scanner1.nextLine();
            String num;
            //en este while mientas que la palabra sea distinta a stop se sseguira haciendo y se utilizara para mostrar el numero aleatorio y escribirlo en fichero
            while(!linea.toLowerCase().equals("stop")){
                bufwri.write(linea);
                bufwri.newLine();
                bufwri.flush();
                num = scanner.nextLine();
                System.out.println(num);
                bfw.write(num);
                bfw.newLine();
                linea = scanner1.nextLine();
            }
            //se cierra el scanner
            scanner1.close();
            //bfw.close();

        }catch(IOException e){
            System.out.println(e.getMessage());
        }
    }
}
