package Ejercicio2;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;


public class Random10 {
    public static void main(String[] args) {
        //se crea una array de integer donde se van guardando los numeros que salen y tambien se crea un scanner
        ArrayList<Integer> numero = new ArrayList<>();
        Scanner input = new Scanner(System.in);
        String palabra = input.nextLine();
        //en este while se comprueba que la palabra introducida sea diferente a stop y si lo es muestra un numero aleatorio y lo guarda en el array
       while(!palabra.toLowerCase().equals("stop") ){
            int numaleatorio = Aleatorio();
            System.out.println(numaleatorio);
            numero.add(numaleatorio);
            palabra = input.nextLine();
        }
    }
    //metodo para deolver un numero aleatorio
    public static int Aleatorio(){
        int random = (int) (Math.random()*10+1);
        return random;
    }
}
