package Ejercicio3;

import java.util.Locale;
import java.util.Scanner;

public class Minusculas {
    public static void main(String[] args) {
        //se crea un scanner para obtener la linea
        Scanner input = new Scanner(System.in);
        String frase = input.nextLine();
        //un while que mientras que la linea sea distinta a finalizar, seguira aceptando palabras y si estan en mayuscula las pasara a minuscula
        while(!frase.toLowerCase().equals("finalizar")){
            System.out.println(frase.toLowerCase());
            frase = input.nextLine();
        }
        //se cierra el scanner
        input.close();
    }
}
