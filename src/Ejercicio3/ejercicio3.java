package Ejercicio3;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ejercicio3 {
    public static void main(String[] args) {
        //en este bloque se le asigna a una string la ruta en la que esta el jar, y conviertes ese string en una array list
        String comando = "java -jar out/artifacts/Ejercicio1_jar2/Ejercicio1.jar";
        List<String> lista = new ArrayList<>(Arrays.asList(comando.split(" ")));
        //se crea el proceso
        ProcessBuilder pb = new ProcessBuilder(lista);


        try{
            //en este bloque se inicializa el proceso
            Process process = pb.start();
            OutputStream outstr = process.getOutputStream();
            OutputStreamWriter outstrwri = new OutputStreamWriter(outstr);
            BufferedWriter bufwri = new BufferedWriter(outstrwri);

            //se crea dos scanners
            Scanner scanner =  new Scanner(process.getInputStream());
            Scanner scanner1 =  new Scanner(System.in);
            String linea = scanner1.nextLine();
            String frase;
            //un while que mientras que la linea sea distinta a finalizar, seguira aceptando palabras y la mostrar en minuscula
            while(!linea.toLowerCase().equals("finalizar")){
                bufwri.write(linea);
                bufwri.newLine();
                bufwri.flush();
                frase = scanner.nextLine();
                System.out.println(frase);
                linea = scanner1.nextLine();
            }
            //se cierra el escaner
            scanner1.close();
            //bfw.close();

        }catch(IOException e){
            System.out.println(e.getMessage());
        }
    }
}
