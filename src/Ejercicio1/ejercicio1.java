package Ejercicio1;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class ejercicio1 {
    public static void main(String[] args) {
        //Array list que contiene los argumentos a ejecutar
        List<String> lista = new ArrayList<>();
        for (int i  = 0; i < args.length;i++ ){
            lista.add(args[i]);
        }
        //try-catch para inicializar el proceso y si el proceso dura as de 2 segundos este se cerrara
        try{
        Process pb = new ProcessBuilder(lista).start();
        Boolean falla = !pb.waitFor(2, TimeUnit.SECONDS);
        //if para saber si el proceso ha tardado mas de 2 segundos, en el leerá la salida del proceso
        if (!falla){

            /*

                Para saber si ha fallado o no se ha de ejecutar exitValue

            */

            InputStream is = pb.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;
            //se crea un ichero en el que escribir
            BufferedWriter bw = new BufferedWriter(new FileWriter("output.txt"));
            //bucle para escribir dentro del fichero anterior los comandos utilizados
            while((line = br.readLine()) !=  null){
                /**
                 * Si se utiliza bufferedwritter mejor así
                 */
                bw.write(line);
                bw.newLine();
            }
            bw.close();
        }else{
            throw new TimeoutException();
        }
        }catch(IOException e){
            System.out.println(e.getMessage());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            System.out.println("Tiempo agotado");
        }
    }

}
